const WaitingList = [
    {
        serial_no: 1, name: 'Ralph', phone_number: 88006644,
        timestamp: new Date(2021, 8, 29, 12, 0, 0),
    },
    {
        serial_no: 2, name: 'Sarah', phone_number: 99001155,
        timestamp: new Date(2021, 8, 29, 12, 30, 10),
    },
    {
        serial_no: 3, name: 'Debs', phone_number: 99220000,
        timestamp: new Date(2021, 8, 29, 13, 0, 20),
    }
  ];

  function GetFreeSlots() {
    return 25 - WaitingList.length;
  }

  function handleSubmit(e) {
    e.preventDefault();
  }

  function DeleteButton() {
    return (
      <form onSubmit={handleSubmit}>
        <button type="submit">Seat Customer</button>
      </form>
    );
  }
  
  class ReminderMessage extends React.Component {
    render() {
      return (
        <div>Please kindly inform guests that the waiting list can only hold up to 25 spots at any one time. If the waiting list is full, guests are welcome to visit again at a later timing.</div>
      );
    }
  }
  
  class WaitingListRow extends React.Component {
    render() {
      const CustomerDetails = this.props.customer;
      return (
        <tr>
          <td>{CustomerDetails.serial_no}</td>
          <td>{CustomerDetails.name}</td>
          <td>{CustomerDetails.phone_number}</td>
          <td>{CustomerDetails.timestamp.toString()}</td>
        </tr>
      );
    }
  }
  
  class DisplayCustomers extends React.Component {
    render() {
      const rows = WaitingList.map(customer =>
        <WaitingListRow key={customer.serial_no} customer={customer} />
      );
  
      return (
        <React.Fragment>
          <b>Waiting List</b><br></br><br></br>
          <table className="bordered-table">
            <thead>
              <tr>
                <th>Serial No.</th>
                <th>Name</th>
                <th>Phone Number</th>
                <th>Timestamp</th>
              </tr>
            </thead>
            <tbody>
              {rows}
            </tbody>
          </table>
        </React.Fragment>
      );
    }
  }

  class DisplayFreeSlots extends React.Component {
    render() {
      return (
          <div>
              Vacancies Left: {GetFreeSlots()}
          </div>
      );
    }
  }

  class AddCustomer extends React.Component {
    render() {
      return (
        <React.Fragment>
        <b>Register a guest</b>
        <form onSubmit={handleSubmit}>
          <table className="borderless-table">
            <thead>
              <tr>
                <td>Guest Name</td>
                <td>
                  <input type="text" placeholder="Danny" id="newGuestName" minLength="1" required/>
                </td>
              </tr>

              <tr>
                <td>Phone Number</td>
                <td>
                    <input type="tel" pattern="[0-9]{8}" placeholder="91234567" minLength="8" maxLength="8" id="newPhoneNumber" required/>
                </td>
              </tr>

              <tr>
                <td><button type="submit">Register Customer</button></td>
              </tr>
            </thead>
          </table>
        </form>
        </React.Fragment>
      );
    }
  }


  // class AddCustomer extends React.Component {
  //   render() {
  //     return (
  //       <div>
  //         <b>Register a new guest</b><br></br>
  //         <form onSubmit={handleSubmit}>
  //           <label for="guestname">Guest Name:</label><br></br>
  //           <input type="text" placeholder="Danny" id="newGuestName" minlength="1" required/><br></br><br></br>
  //           <label for="phonenumber">Phone Number:</label><br></br>
  //           <input type="tel" pattern="[0-9]{8}" placeholder="91234567" minlength="8" maxlength="8" id="newPhoneNumber" required/>
  //           <button type="submit">Register Customer</button>
  //         </form>
  //       </div>  
  //     );
  //   }
  // }

  class DeleteCustomer extends React.Component {
    render() {
      return (
        <React.Fragment>
        <b>Seat a guest</b>
        <form onSubmit={handleSubmit}>
          <table className="borderless-table">
            <thead>
              <tr>
                <td>Phone Number</td>
                <td>
                    <input type="tel" pattern="[0-9]{8}" placeholder="91234567" minLength="8" maxLength="8" id="newPhoneNumber" required/>
                </td>
              </tr>

              <tr>
                <td><button type="submit">Seat a Customer</button></td>
              </tr>
            </thead>
          </table>
        </form>
      </React.Fragment>
      );
    }
  }
  
  class DisplayHomepage extends React.Component {
    render() {
      return (
        <React.Fragment>
          <h1>Hotel California</h1>
          <ReminderMessage />
          <hr />
          <AddCustomer /><br></br>
          <DeleteCustomer />
          <hr />
          <DisplayCustomers /><br></br>
          <DisplayFreeSlots />
        </React.Fragment>
      );
    }
  }
  
  const element = <DisplayHomepage />;
  
  ReactDOM.render(element, document.getElementById('contents'));