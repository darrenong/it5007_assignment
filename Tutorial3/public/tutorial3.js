const WaitingList = [{
  serial_no: 1,
  name: 'Ralph',
  phone_number: 88006644,
  timestamp: new Date(2021, 8, 29, 12, 0, 0)
}, {
  serial_no: 2,
  name: 'Sarah',
  phone_number: 99001155,
  timestamp: new Date(2021, 8, 29, 12, 30, 10)
}, {
  serial_no: 3,
  name: 'Debs',
  phone_number: 99220000,
  timestamp: new Date(2021, 8, 29, 13, 0, 20)
}];

function GetFreeSlots() {
  return 25 - WaitingList.length;
}

function handleSubmit(e) {
  e.preventDefault();
}

function DeleteButton() {
  return /*#__PURE__*/React.createElement("form", {
    onSubmit: handleSubmit
  }, /*#__PURE__*/React.createElement("button", {
    type: "submit"
  }, "Seat Customer"));
}

class ReminderMessage extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement("div", null, "Please kindly inform guests that the waiting list can only hold up to 25 spots at any one time. If the waiting list is full, guests are welcome to visit again at a later timing.");
  }

}

class WaitingListRow extends React.Component {
  render() {
    const CustomerDetails = this.props.customer;
    return /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("td", null, CustomerDetails.serial_no), /*#__PURE__*/React.createElement("td", null, CustomerDetails.name), /*#__PURE__*/React.createElement("td", null, CustomerDetails.phone_number), /*#__PURE__*/React.createElement("td", null, CustomerDetails.timestamp.toString()));
  }

}

class DisplayCustomers extends React.Component {
  render() {
    const rows = WaitingList.map(customer => /*#__PURE__*/React.createElement(WaitingListRow, {
      key: customer.serial_no,
      customer: customer
    }));
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("b", null, "Waiting List"), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("table", {
      className: "bordered-table"
    }, /*#__PURE__*/React.createElement("thead", null, /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("th", null, "Serial No."), /*#__PURE__*/React.createElement("th", null, "Name"), /*#__PURE__*/React.createElement("th", null, "Phone Number"), /*#__PURE__*/React.createElement("th", null, "Timestamp"))), /*#__PURE__*/React.createElement("tbody", null, rows)));
  }

}

class DisplayFreeSlots extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement("div", null, "Vacancies Left: ", GetFreeSlots());
  }

}

class AddCustomer extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("b", null, "Register a guest"), /*#__PURE__*/React.createElement("form", {
      onSubmit: handleSubmit
    }, /*#__PURE__*/React.createElement("table", {
      className: "borderless-table"
    }, /*#__PURE__*/React.createElement("thead", null, /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("td", null, "Guest Name"), /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement("input", {
      type: "text",
      placeholder: "Danny",
      id: "newGuestName",
      minLength: "1",
      required: true
    }))), /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("td", null, "Phone Number"), /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement("input", {
      type: "tel",
      pattern: "[0-9]{8}",
      placeholder: "91234567",
      minLength: "8",
      maxLength: "8",
      id: "newPhoneNumber",
      required: true
    }))), /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement("button", {
      type: "submit"
    }, "Register Customer")))))));
  }

} // class AddCustomer extends React.Component {
//   render() {
//     return (
//       <div>
//         <b>Register a new guest</b><br></br>
//         <form onSubmit={handleSubmit}>
//           <label for="guestname">Guest Name:</label><br></br>
//           <input type="text" placeholder="Danny" id="newGuestName" minlength="1" required/><br></br><br></br>
//           <label for="phonenumber">Phone Number:</label><br></br>
//           <input type="tel" pattern="[0-9]{8}" placeholder="91234567" minlength="8" maxlength="8" id="newPhoneNumber" required/>
//           <button type="submit">Register Customer</button>
//         </form>
//       </div>  
//     );
//   }
// }


class DeleteCustomer extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("b", null, "Seat a guest"), /*#__PURE__*/React.createElement("form", {
      onSubmit: handleSubmit
    }, /*#__PURE__*/React.createElement("table", {
      className: "borderless-table"
    }, /*#__PURE__*/React.createElement("thead", null, /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("td", null, "Phone Number"), /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement("input", {
      type: "tel",
      pattern: "[0-9]{8}",
      placeholder: "91234567",
      minLength: "8",
      maxLength: "8",
      id: "newPhoneNumber",
      required: true
    }))), /*#__PURE__*/React.createElement("tr", null, /*#__PURE__*/React.createElement("td", null, /*#__PURE__*/React.createElement("button", {
      type: "submit"
    }, "Seat a Customer")))))));
  }

}

class DisplayHomepage extends React.Component {
  render() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("h1", null, "Hotel California"), /*#__PURE__*/React.createElement(ReminderMessage, null), /*#__PURE__*/React.createElement("hr", null), /*#__PURE__*/React.createElement(AddCustomer, null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(DeleteCustomer, null), /*#__PURE__*/React.createElement("hr", null), /*#__PURE__*/React.createElement(DisplayCustomers, null), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement(DisplayFreeSlots, null));
  }

}

const element = /*#__PURE__*/React.createElement(DisplayHomepage, null);
ReactDOM.render(element, document.getElementById('contents'));